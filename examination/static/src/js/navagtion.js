$(document).ready(function() 
{
	// A Question Paper
	var the_form = $('.js_surveyform');
		
	// Json Prefill
	var prefill_controller = the_form.attr("data-prefill");
	
    var validate_controller = the_form.attr("data-validate");
    
    var submit_controller = the_form.attr("data-submit");
    
    var scores_controller = the_form.attr("data-scores");
    
    var ans_controller = the_form.attr("data-ans");
    
    var survey=$('.survey').val();
	var token=$('.token').val();
	
	
	/* This Function for create Navigation panel ( ajax method ) */
    $('.nav_row').html("");
    function get_data() 
	{
    	
    	openerp.jsonRpc("/exam_nav", 'call', {'survey':survey,'token':token}).then(function (json_data)
		{
			var page_no = json_data["page"];
			var question_no = json_data["question"];
			var pages=json_data["pageids"];
			var questions=json_data["questionids"];
			for (var j = 0; j < question_no; j++) 
			{
				
				$('.nav_row').append("<div style='float:left;padding:0px;margin:15px 10px;background-color:white;'>" +
								"<input type='button' class='btn_nav'  ID='btn' value='"+(j+1)+"' page-id='"+pages[j]+"'question-id='"+questions[j]+"'>")
			}
		})
		.fail(function()
		{
             console.warn("Some Error");
		 });
	}
    
    /* This Function for Get the ans into Navigation panel & display count ( ajax method ) */ 
	function get_ans() 
	{
		//get_data();
		console.log("start");
		openerp.jsonRpc(ans_controller, 'call', {}).then(function (json_data)
		{
			console.log("load")
			var page_count = json_data["count"];
			$('#ans_count').html(page_count + ' / 40');
			var pages = json_data["ans_input_line_data"]
			for (j = 0; j < page_count; j++) 
			{
				var pageid=pages[j];
				$( "input[page-id='"+pageid+"']" ).addClass('ans_nav_btn');
			}
			var pageid_nav=$('.page').val();
			$("input[page-id='"+pageid_nav+"']" ).addClass('active_nav_btn');
			var current_que_no =$("input[page-id='"+pageid_nav+"']" ).val(); //$("input.active_nav_btn").val();
                        console.log(current_que_no);
			if(current_que_no != undefined){
				$('.question_number').html(current_que_no + '.');
			}
		})
		.fail(function()
		{
             console.log("Some Error");
		});
	}
	
	
	
	/* This Function is Prefill the Ans which given by user */
	function prefill(){
        if (! _.isUndefined(prefill_controller)) {
            var prefill_def = $.ajax(prefill_controller, {dataType: "json"})
                .done(function(json_data){
                    $.each(json_data,function(value, key)
                    {		
                        console.log(the_form.find("input[name='" + value + "'][type!='text']"))
                        the_form.find("input[name='" + value + "'][type!='text']").each(function()
                        {
                           $(this).val(key)
                           
                        });
                    });
                })
                .fail(function(){
                    console.warn("[survey] Unable to load prefill data");
                });
            return prefill_def;
        }
    }
	
	prefill();
	
	$('.nav_row').on('click', '#btn', function()
	{
		
	    var page_click_id=$(this).val();
	    var current_pg = $('.page').val();
	    var tokenno = $('.token').val();
	    the_form.find('.page_nav_id').val(page_click_id)
	    the_form.submit();
	});
	
	$('.btnend').click(function()
	{
		  console.log("timeend")	
		  clearTimeout(interval);
		  localStorage.removeItem("end1");
		  localStorage.clear();
	});
	
	
	
	
	
	/* This is submit the ans on prev & Next Button Click */
	$('.js_surveyform').ajaxForm({
        url: submit_controller,
        type: 'POST',
        dataType: 'json',                   							// answer expected type
        beforeSubmit: function(){           							// hide previous errmsg before
																		// resubmitting
            $('.js_errzone').html("").hide();
           
        },
        success: function(response, status, xhr, wfe){ 					// submission attempt // some questions have errors
        	if(_.has(response, 'errors'))
        	{  							
        		//alert(response)
                return false;
            }
            else if (_.has(response, 'redirect')){      				// form is ok
            	window.location.replace(response.redirect);
                return true;
            }
            else {                                      				// server sends bad data
                console.error("Incorrect answer sent by server");
                return false;
            }
        },
        timeout: 5000,
        error: function(jqXHR, textStatus, errorThrown){ 				// failure of AJAX request		
        }
    });
	
	
	if($('.question-div-main').innerHeight() >= 500){
		$('.more_data').css('display','block');
	}
	else{
		$('.more_data').css('display','none');
	}
	$('.more_data i').click(function() {
		$(".question-div-main").animate({ scrollTop: $(document).height() }, 1000);
	});
        get_data();
	setTimeout(function(){
		get_ans();
	},100);
	
})
