$(document).ready(function() {
	$('.btn').click(function() {
		if ($("#loginid").val() == '') {
			$('.error_msg').css('display','block');
		}
		else{
			$('.error_msg').css('display','none');
		}
		if ($(".domain-dropdown option:selected").text() == 'Select'){
			$('.error_msg_select').css('display','block');
			return false;
		}
		else{
			$('.error_msg_select').css('display','none');
		}
		return true;
	});
	$('#loginid').focusout(function(){
		if ($("#loginid").val() == '') {
			$('.error_msg').css('display','block');
			return false;
		}
		else{
			$('.error_msg').css('display','none');
		}
	})
	$('.domain-dropdown').focusout(function(){
		if ($(".domain-dropdown option:selected").text() == 'Select'){
			$('.error_msg_select').css('display','block');
			return false;
		}
		else{
			$('.error_msg_select').css('display','none');
		}
	})
	
	
	$(document).keydown(function(event){
		if(event.keyCode == 123) {
			return false;
		}
	});
	window.oncontextmenu = function ()
	{
	    return false;
	}
});
