// Default JS Timer 
//var end = new Date('MM/DD/YYYY HH:MM PM/AM');
var end = new Date('07/18/2017 7:30	 PM');
/*var end = new Date();
end.setMinutes(end.getMinutes() + 120)*/
var _second = 1000;
var _minute = _second * 60;
var _hour = _minute * 60;
var _day = _hour * 24;			
var timer;
function showRemainingTime() 
{
    var now = new Date();
    var distance = end - now;
    if (distance < 0) 
    {

        clearInterval(timer);
        var expired_model = $("#model");
        expired_model.css('display','block')
        var close_btn = $(".close_btn button")[0];
        $('#countdown').html('EXPIRED !');
        close_btn.onclick = function()
        {
        	expired_model.css('display','none');
        	//$('.js_surveyform').submit();
		}
    }
    else
    {
    	var days = Math.floor(distance / _day);
    	var hours = Math.floor((distance % _day) / _hour);
    	var minutes = Math.floor((distance % _hour) / _minute);
    	var seconds = Math.floor((distance % _minute) / _second);

    	$('#countdown').html(hours + ' Hrs '+minutes + ' Mins '+seconds + ' Secs '+'<span style="font-family:Muli;font-size:18px;margin:0px;color:#2b2b2b">left</span>');
    	$('#countdown1').html(hours + ' Hrs '+minutes + ' Mins '+seconds + ' Secs '+'<span style="font-family:Muli-bold;font-size:18px;margin:0px;color:white">left</span>');
    
    }
}
// ON Net EX 3
var Clock = {
		  totalSeconds: 0,
		  delay_time:0,
		  
		  start: function (delay_time) {
		    var self = this;
		    if(delay_time)
		    {
		    	alert("")
		    	end+=delay_time
		    }
		    	this.interval = setInterval(function () {
		    	var now = new Date();
		    	var distance = end - now;
		    	var days = Math.floor(distance / _day);
		    	var hours = Math.floor((distance % _day) / _hour);
		    	var minutes = Math.floor((distance % _hour) / _minute);
		    	var seconds = Math.floor((distance % _minute) / _second);

		    	$('#countdown').html(hours + ' Hrs '+minutes + ' Mins '+seconds + ' Secs '+'<span style="font-family:Muli;font-size:18px;margin:0px;color:#2b2b2b">left</span>');
		    }, 1000);
		  },

		  pause: function () {
		    clearInterval(this.interval);
		    delete this.interval;
		  },
		  
		  delay: function () {
				var self = this;
				this.interval_temp = setInterval(function () {
					self.delay_time += 1;
					console.log(self.delay_time)
				}, 1000);
			},
			
			stop_delay: function () {
				clearInterval(this.interval_temp);
			    delete this.interval_temp;
			},
		  resume: function () {
		    if (!this.interval) this.start(self.delay_time);
		  }
};
// Json Python Timer 
//to get server status each min and display time and resume with last time when it stop
var t=""
function showRemaining()
{	
	openerp.jsonRpc("/timer", 'call', {'last_time':t}).then(function (data) 
    {
		$(".cus_theme_loader_layout").addClass("hidden")
		$(".stop_error").addClass("hidden");
		t=$('#ct').val();
    	$('#ct').val(data.curr);
    	var ttr=data.time;
    	var tr=ttr.split(":");
    	var hours=tr[0];
   		var minutes=tr[1];
   		var seconds=tr[2];
    	
    	// tO submit the test when time is over ... :(
    	/*if (seconds <= 0) 
    	{
        clearInterval(timer);
        var expired_model = $("#model");
        expired_model.css('display','block')
        var close_btn = $(".close_btn button")[0];
        $('#countdown').html('EXPIRED !');
        	close_btn.onclick = function()
        	{
        	expired_model.css('display','none');
			}
    	}*/
    	
    	//Expriment 2
    	//clearInterval(timer);
   		
   		//Expriment 3
   		//Clock.resume()
   		//Clock.stop_delay()
    	//$('#countdown').html(hours+":"+minutes+":"+seconds+" Remaining")
    	//$('#countdown').html(data.time +" Secs Remaining")
    })
    .fail(function(e)
    {	
    	$(".cus_theme_loader_layout").removeClass("hidden");
    	//Clock.pause()
    	//Clock.delay()
    	$(".stop_error").removeClass("hidden");
    	
    });
}
//Clock.start();
//timer = setInterval(showRemainingTime, 1000);
//time=setInterval(showRemaining, 1000);

